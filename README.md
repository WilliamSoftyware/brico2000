# flowscan

Devflow - M1 Web Project

Members : 

- BOUTTIER Elysa

- DJABIRI Ambasse 

- LE BIHAN Thomas

- CARTIER-MICHAUD Jérémy

- DECHAMPS William

- LEKEDJI ZAHOUN Brucie Francelle


## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

## First commande :
- flutter pub get

## Execute Application with :
- flutter run

## Execute Tests Application with :
- flutter test

## Classes

libs/state_machine.dart & libs/main.dart => Début machine à états

libs/basket.dart & libs/basket_sale.dart & product_base.dart & product_sale.dart & test/widget_test.dart => Début Buisiness Logic
