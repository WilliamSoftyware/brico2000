import 'package:flowscan/models/cart.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  test('buySinglePriceItem', () {
    Cart _jeremy = Cart();
    _jeremy.addProduct(1234, 10);
    expect(_jeremy.getTotalPrice(), 24.0);
    expect(_jeremy.getCartSize(), 1);
    expect(_jeremy.shoppingCart.containsKey(1234), true);
    var _result = _jeremy.shoppingCart.values.toList();
    expect(_result[0], 10);
  });

  test('buyMultiplePriceItem', () {
    Cart _ambasse = Cart();
    _ambasse.addProduct(1234, 5);
    _ambasse.addProduct(2345, 3);
    expect(_ambasse.getTotalPrice(), 26.4);
    expect(_ambasse.getCartSize(), 2);
    expect(_ambasse.shoppingCart.containsKey(1234), true);
    expect(_ambasse.shoppingCart.containsKey(2345), true);
    var _result = _ambasse.shoppingCart.values.toList();
    expect(_result[0], 5);
    expect(_result[1], 3);
  });

  test('buyPriceItemThatNoInCart', () {
    Cart _quentin = Cart();
    _quentin.addProduct(1234, 5);
    _quentin.addProduct(1111, 5);
    expect(_quentin.getTotalPrice(), 12.0);
    expect(_quentin.getCartSize(), 1);
    expect(_quentin.shoppingCart.containsKey(1234), true);
    var _result = _quentin.shoppingCart.values.toList();
    expect(_result[0], 5);
  });

  test('buyNegativeQuantityItem', () {
    Cart _william = Cart();
    _william.addProduct(1234, -5);
    expect(_william.getTotalPrice(), 0);
    expect(_william.getCartSize(), 0);
    expect(_william.shoppingCart.containsKey(1234), false);
  });

  test('cleanShoppingCartThenBuy', () {
    Cart _thomas = Cart();
    _thomas.addProduct(1234, 7);
    _thomas.addProduct(2345, 3);
    _thomas.addProduct(3456, 5);
    _thomas.addProduct(4567, 10);
    _thomas.cleanShoppingCart();
    _thomas.addProduct(1234, 5);
    expect(_thomas.getCartSize(), 1);
    expect(_thomas.shoppingCart.containsKey(1234), true);
    var _result = _thomas.shoppingCart.values.toList();
    expect(_result[0], 5);
    expect(_thomas.getTotalPrice(), 12.0);
  });

  test('putSomeItemsBack', () {
    Cart _elysa = Cart();
    _elysa.addProduct(1234, 5);
    _elysa.addProduct(3456, 5);
    _elysa.putBack(3456, 2);
    expect(_elysa.getCartSize(), 2);
    expect(_elysa.shoppingCart.containsKey(1234), true);
    expect(_elysa.shoppingCart.containsKey(3456), true);
    var _result = _elysa.shoppingCart.values.toList();
    expect(_result[0], 5);
    expect(_result[1], 3);
    expect(_elysa.getTotalPrice(), 19.2);
  });

  test('putSomeItemsBackThatNoInCart', () {
    Cart _anthony = Cart();
    _anthony.addProduct(1234, 5);
    _anthony.putBack(1111, 2);
    expect(_anthony.getCartSize(), 1);
    expect(_anthony.shoppingCart.containsKey(1234), true);
    var _result = _anthony.shoppingCart.values.toList();
    expect(_result[0], 5);
    expect(_anthony.getTotalPrice(), 12.0);
  });

  test('changeItemsQuantity', () {
    Cart _brucie = Cart();
    _brucie.addProduct(1234, 1);
    _brucie.changeQuantity(1234, 5);
    expect(_brucie.getCartSize(), 1);
    expect(_brucie.shoppingCart.containsKey(1234), true);
    var _result = _brucie.shoppingCart.values.toList();
    expect(_result[0], 5);
    expect(_brucie.getTotalPrice(), 12.0);
  });

  test('changeItemsQuantityThatNotInCart', () {
    Cart _sophie = Cart();
    _sophie.addProduct(1234, 6);
    _sophie.changeQuantity(1111, 5);
    _sophie.putBack(1234, 1);
    expect(_sophie.getTotalPrice(), 12.0);
    expect(_sophie.getCartSize(), 1);
    expect(_sophie.shoppingCart.containsKey(1234), true);
    var _result = _sophie.shoppingCart.values.toList();
    expect(_result[0], 5);
  });

  test('getProductListByBasketCode product exists', () {
    Cart _tristan = Cart();
    _tristan.addProduct(1234, 1);
    _tristan.addProduct(2345, 1);
    _tristan.addProduct(3456, 1);
    expect(_tristan.getCartSize(), 3);
    expect(_tristan.shoppingCart.containsKey(1234), true);
    expect(_tristan.shoppingCart.containsKey(2345), true);
    expect(_tristan.shoppingCart.containsKey(3456), true);
    var _result = _tristan.shoppingCart.values.toList();
    expect(_result[0], 1);
    expect(_result[1], 1);
    expect(_result[2], 1);
  });

  test('getProductListByBasketCode produit dont exists', () {
    Cart _julie = Cart();
    _julie.addProduct(1234, 3);
    _julie.addProduct(2345, 1);
    _julie.addProduct(3456, 1);
    _julie.addProduct(1111, 1);
    expect(_julie.getCartSize(), 3);
    expect(_julie.shoppingCart.containsKey(1234), true);
    expect(_julie.shoppingCart.containsKey(2345), true);
    expect(_julie.shoppingCart.containsKey(3456), true);
    expect(_julie.shoppingCart.containsKey(1111), false);
    var _result = _julie.shoppingCart.values.toList();
    expect(_result[0], 3);
    expect(_result[1], 1);
    expect(_result[2], 1);
  });

  test('payCart equality amount one time', () {
    // (2euros * 1produits) = 2euros + 2 euros de TVA = 3
    Cart _lucie = Cart();
    _lucie.addProduct(1234, 1);
    // Paie 6 euros
    _lucie.payCart(6);
    // _total = -3.6 on doit 3.6 euros à lucie...
    // A voir comment améliorer ..
    expect(_lucie.getTotalPrice(), -3.6);
    expect(_lucie.getCartStatus(), true);
  });

  test('payCart equality amount several times', () {
    // (2euros * 5produits) = 10euros + 2 euros de TVA = 12
    Cart _lucie = Cart();
    _lucie.addProduct(1234, 1);
    _lucie.changeQuantity(1234, 5);
    expect(_lucie.getCartStatus(), false);
    // Paie 6 euros
    _lucie.payCart(6);
    // Il reste 6 euros à payer
    expect(_lucie.getCartStatus(), false);
    expect(_lucie.getTotalPrice(), 6);
    // Paie 6 euros
    _lucie.payCart(6);
    expect(_lucie.getTotalPrice(), 0);
    expect(_lucie.getCartStatus(), true);
  });

  test(
      'buy 19 products with the same code, but there are only 12 products in the catalog',
      () {
    Cart _julie = Cart();
    _julie.addProduct(1234, 19);
    var productList = _julie.getProductList();
    expect(_julie.getCartSize(), 1);
    expect(
        _julie.catalog.getProductQuantityByCode(productList[0].productCode), 0);
    expect(_julie.getQuantity(productList[0].productCode), 12);
    expect(_julie.getTotalPrice(), 28.8);
  });

  test("get categoryByProductCode return 'Fruits'", () {
    Cart _dracofeu = Cart();
    var getCategoryByPC = _dracofeu.catalog.getCategoryByProductCode(1234)!;
    expect(getCategoryByPC.name, "Fruits");
    expect(
        _dracofeu.catalog.getProductsByCategoryCode("FruitsA"),
        _dracofeu.catalog
            .getCategories()
            .firstWhere((element) =>
                element.getCategoryCode() == getCategoryByPC.categoryCode)
            .getProductsInCateg());
  });

  test("get wrong categoryByProductCode return null", () {
    Cart _pikachu = Cart();
    expect(_pikachu.catalog.getCategoryByProductCode(1111), null);
    expect(_pikachu.catalog.getProductsByCategoryCode("FruitsB"), null);
  });
}
