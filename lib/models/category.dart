import 'package:flowscan/models/product.dart';

class CategoryP {
  late final String categoryCode;
  String name;
  String description;
  List<ProductBase> productsInCateg;

  CategoryP(
      this.categoryCode, this.name, this.description, this.productsInCateg);

  String getCategoryCode() {
    return categoryCode;
  }

  String getProductName() {
    return name;
  }

  String getDescription() {
    return description;
  }

  void addProduct(ProductBase productToAdd) {
    productsInCateg.add(productToAdd);
  }

  void removeProduct(int? productCode, ProductBase? product) {
    if (productCode != null) {
      productsInCateg
          .removeWhere((product) => product.productCode == productCode);
    } else {
      productsInCateg.remove(product);
    }
  }

  List<ProductBase> getProductsInCateg() {
    return productsInCateg;
  }
}
