import 'package:flowscan/models/catalog.dart';
import 'package:flowscan/models/product.dart';
import 'package:flutter/foundation.dart';
import 'package:uuid/uuid.dart';

class Cart extends ChangeNotifier {
  late String cartCode;
  late Catalog catalog;
  late double price;
  late double amountPaid;
  bool paid = false;
  late Map<int, int> shoppingCart;
  late List<ProductBase> productList;

  Cart() {
    catalog = Catalog();
    cartCode = const Uuid().v1().toString().substring(0, 13);
    price = 0;
    amountPaid = 0;
    // Add one banana and 2 potatoes for tests
    shoppingCart = {};
    updateProductList();
    productList = getProductList();
    notifyListeners();
  }

  double getTotalPrice() {
    checkOut();
    return price;
  }

  /// Returns cartCode.
  String getcartCode() {
    return cartCode;
  }

  /// Returns the number of product in shoppingCart.
  int getCartSize() {
    return shoppingCart.length;
  }

  /// Returns the status of the shoppingCart (true = paid, false = not paid).
  bool getCartStatus() {
    return paid;
  }

  /// Returns list of produtBase (products in cart).
  List<ProductBase> getProductList() {
    return productList;
  }

  /// Update list products in shopping card.
  void updateProductList() {
    productList = [];
    for (var product in shoppingCart.entries) {
      var productFound = catalog.getProductByCode(product.key);
      if (productFound != null) {
        productFound.quantity = product.value;
        if (productFound.quantity <= 0) {
          productList.remove(productFound);
        } else {
          productList.add(productFound);
        }
      }
    }
  }

  /// Puts product in shopping cart. Need a product's Id(barcode) and the quantity. Returns total price - amountPaid.
  void addProduct(int productCode, int productQuantity) {
    var productFound = catalog.getProductByCode(productCode);
    if (productFound == null ||
        productQuantity.isNegative ||
        productQuantity.isNaN ||
        productQuantity.isInfinite) {
      return;
    }
    // TODO: Stocks management
    //var rest = catalog.pullProductQuantity(productFound, productQuantity);
    if (shoppingCart.containsKey(productCode)) {
      // if (rest < 0) {
      shoppingCart[productCode] =
          shoppingCart[productCode]! + (productQuantity);
      // } else {
      //   shoppingCart[productCode] =
      //       shoppingCart[productCode]! + productQuantity;
      // }
    } else {
      // if (rest < 0) {
      shoppingCart[productCode] = productQuantity;
      // } else {
      //   shoppingCart[productCode] = productQuantity;
      // }
    }
    updateProductList();
    notifyListeners();
  }

  /// Puts product back to shelf. Need a product's Id(barcode) and the quantity. Returns total price - amountPaid.
  void putBack(int productCode, int productQuantity) {
    var productFound = catalog.getProductByCode(productCode);
    if (productFound == null ||
        productQuantity.isNegative ||
        productQuantity.isNaN ||
        productQuantity.isInfinite) {
      return;
    }
    if (shoppingCart.containsKey(productCode) &&
        shoppingCart[productCode]! - productQuantity >= 0) {
      // TODO: Stocks management
      // catalog.addProductQuantity(productFound, productQuantity);
      shoppingCart[productCode] = shoppingCart[productCode]! - productQuantity;
    }
    updateProductList();
    notifyListeners();
  }

  /// Returns quantity items in card for a product's code given (barcode). Otherwise returns null.
  int? getQuantity(int productCode) {
    for (var product in shoppingCart.entries) {
      if (product.key == productCode) {
        return product.value;
      }
    }
    return null;
  }

  /// Changes quantity items in card. Need a product's Id(barcode) and the quantity.
  void changeQuantity(int productCode, int productQuantity) {
    var productFound = catalog.getProductByCode(productCode);
    if (productFound == null ||
        productQuantity.isNegative ||
        productQuantity.isNaN ||
        productQuantity.isInfinite) {
      return;
    }
    // TODO: Stocks management
    // var rest = catalog.pullProductQuantity(productFound, productQuantity);
    if (shoppingCart.containsKey(productCode)) {
      shoppingCart[productCode] = productQuantity;
      if (productQuantity == 0) {
        shoppingCart.remove(productCode);
      }
    }
    updateProductList();
    notifyListeners();
  }

  /// Returns a ProductBase for a productCode given. Otherwise returns null.
  ProductBase? getProductByCode(int productCode) {
    for (var product in shoppingCart.entries) {
      var productFound = catalog.getProductByCode(product.key);
      if (productFound == null) {
        return null;
      }
      if (productFound.productCode == productCode) {
        return productFound;
      }
    }
    return null;
  }

  /// Gives up this shopping cart, pick a new shopping cart.
  void cleanShoppingCart() {
    cartCode = const Uuid().v1().toString().substring(0, 13);
    price = 0;
    amountPaid = 0;
    shoppingCart = {};
    updateProductList();
    notifyListeners();
  }

  /// Calculates and return total price for one product.
  double getTotalProductPrice(int productCode) {
    double productPrice = 0.00;
    for (var product in shoppingCart.entries) {
      var selectResult = catalog.getProductByCode(product.key);
      if (selectResult == null) {
        break;
      }
      if (selectResult.productCode == productCode) {
        var stringVtaPrice =
            ((selectResult.getProductPrice() * product.value) * 1.2)
                .toStringAsFixed(2);
        productPrice += double.parse(stringVtaPrice);
      }
    }
    return productPrice;
  }

  /// Calculates and return total price to paid - amount paid.
  void checkOut() {
    price = 0;
    for (var product in shoppingCart.entries) {
      var selectResult = catalog.getProductByCode(product.key);
      if (selectResult == null) {
        break;
      }
      var stringVtaPrice =
          ((selectResult.getProductPrice() * product.value) * 1.2)
              .toStringAsFixed(2);
      price += double.parse(stringVtaPrice);
    }
    price = price - amountPaid;
  }

  /// Pays a part of total price to paid.
  double payCart(double amount) {
    if (amount.isInfinite || amount.isNaN || amount.isNegative) {
      return updatePricePaid();
    }
    amountPaid += amount;
    return updatePricePaid();
  }

  /// Updates paid state depending on total price to paid and the amount paid.
  double updatePricePaid() {
    checkOut();
    if (price > 0) {
      paid = false;
    }
    if (price <= 0) {
      paid = true;
    }
    return price;
  }
}
