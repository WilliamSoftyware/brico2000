import 'package:flowscan/models/category.dart';

class ProductBase {
  late final int productCode;
  String name;
  CategoryP? category;
  double price;
  int quantity;

  ProductBase(
      this.productCode, this.name, this.price, this.category, this.quantity);

  int getProductCode() {
    return productCode;
  }

  String getProductName() {
    return name;
  }

  double getProductPrice() {
    return price;
  }

  int getProductQuantity() {
    return quantity;
  }

  CategoryP? getCategory() {
    return category;
  }
}
