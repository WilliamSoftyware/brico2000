import 'package:flowscan/models/category.dart';
import 'package:flowscan/models/product.dart';
import 'package:collection/collection.dart';
import 'package:flutter/foundation.dart';

class Catalog extends ChangeNotifier {
  List<ProductBase> productList = [];
  List<CategoryP> categoryList = [];

  Catalog() {
    _initProductList();
  }

  void _initProductList() {
    categoryList = [
      CategoryP("FruitsA", "Fruits", "Des fruits..", []),
      CategoryP("LégumesA", "Légumes", "Des légumes..", []),
      CategoryP("ViandesA", "Viandes", "Des viandes...", [])
    ];
    productList = [
      ProductBase(1234, 'Banane', 2.00, categoryList[0], 2),
      ProductBase(2345, 'Patate', 4.00, categoryList[1], 16),
      ProductBase(3456, 'Fraise', 2.00, categoryList[0], 14),
      ProductBase(4567, 'Pomme', 6.00, categoryList[0], 19),
      ProductBase(5678, 'Courgette', 1.00, categoryList[2], 12),
      ProductBase(7890, 'Haricot', 3.00, categoryList[1], 19),
      ProductBase(8901, 'Choux', 4.00, categoryList[1], 122),
      ProductBase(9000, 'Poulet', 4.00, categoryList[2], 32),
      ProductBase(9001, 'Boeuf', 1.99, categoryList[2], 41)
    ];
    categoryList[0].productsInCateg.add(productList[0]);
    categoryList[0].productsInCateg.add(productList[2]);
    categoryList[0].productsInCateg.add(productList[3]);
    categoryList[1].productsInCateg.add(productList[1]);
    categoryList[1].productsInCateg.add(productList[5]);
    categoryList[1].productsInCateg.add(productList[6]);
    categoryList[2].productsInCateg.add(productList[4]);
    categoryList[2].productsInCateg.add(productList[7]);
    categoryList[2].productsInCateg.add(productList[8]);
  }

  /// Returns CategoryP if exist, else return null. Need categoryCode.
  CategoryP? isInCategory(String categoryCode) {
    return categoryList.firstWhereOrNull(
        (category) => category.getCategoryCode() == categoryCode);
  }

  /// Adds product in catalog. Need a ProductBase.
  void addProduct(ProductBase product) {
    productList.add(product);
  }

  /// Adds product in catalog. Need a ProductBase.
  void removeProduct(ProductBase product) {
    productList.remove(product);
  }

  /// Updates quantity for one product in catalog. Need ProductBase & quantity.
  void changeProductQuantity(ProductBase product, int quantity) {
    var productFound = productList.firstWhereOrNull(
        (productBase) => productBase.productCode == product.productCode);
    if (productFound == null) {
      return;
    }
    productFound.quantity = quantity;
    if (productFound.quantity < 0) {
      productFound.quantity = 0;
    }
    var index = productList.indexOf(productFound);
    productList[index] = productFound;
  }

  /// Adds quantity for one product in catalog. Need ProductBase & quantity.
  int? addProductQuantity(ProductBase product, int quantity) {
    var rest = 0;
    var productFound = productList.firstWhereOrNull(
        (productBase) => productBase.productCode == product.productCode);
    if (productFound == null) {
      return 0;
    }
    productFound.quantity += quantity;
    rest = productFound.quantity;

    if (productFound.quantity < 0) {
      productFound.quantity = 0;
    }
    var index = productList.indexOf(productFound);
    productList[index] = productFound;
    return rest;
  }

  /// Returns rest quantity for one product in catalog. Need ProductBase & quantity to pull.
  int pullProductQuantity(ProductBase product, int quantity) {
    var rest = 0;
    var productFound = productList.firstWhereOrNull(
        (productBase) => productBase.productCode == product.productCode);
    if (productFound == null) {
      return 0;
    }
    productFound.quantity -= quantity;
    rest = productFound.quantity;

    if (productFound.quantity < 0) {
      productFound.quantity = 0;
    }
    var index = productList.indexOf(productFound);
    productList[index].quantity = productFound.quantity;
    notifyListeners();
    return rest;
  }

  /// Returns list of ProductBase in catalog.
  List<ProductBase> getCatalog() {
    return productList;
  }

  List<CategoryP> getCategories() {
    return categoryList;
  }

  /// Returns product's quantity in catalog. Need productCode.
  int? getProductQuantityByCode(int productCode) {
    return productList
        .firstWhere(
            (productBase) => productBase.getProductCode() == productCode)
        .quantity;
  }

  /// Returns ProductBase if exist, else return null. Need productCode.
  ProductBase? getProductByCode(int productCode) {
    return productList.firstWhereOrNull(
        (productBase) => productBase.getProductCode() == productCode);
  }

  CategoryP? getCategoryByProductCode(int productCode) {
    var productFound = getProductByCode(productCode);
    if (productFound == null) {
      return null;
    }
    return productFound.getCategory();
  }

  List<ProductBase>? getProductsByCategoryCode(String categoryCode) {
    var categoryFound = isInCategory(categoryCode);
    if (categoryFound == null) {
      return null;
    }
    return categoryFound.getProductsInCateg();
  }
}
