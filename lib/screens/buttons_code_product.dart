import 'package:flowscan/models/cart.dart';
import 'package:flowscan/paths_app.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class RefArticle extends StatefulWidget {
  const RefArticle({Key? key}) : super(key: key);
  @override
  _RefArticleState createState() => _RefArticleState();
}

class _RefArticleState extends State<RefArticle> {
  final List<String> buttons = [
    "7",
    "8",
    "9",
    "4",
    "5",
    "6",
    "1",
    "2",
    "3",
    "X",
    "0",
    "->"
  ];

  // Create a text controller and use it to retrieve the current value
  // of the TextField.
  final myController = TextEditingController();

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    myController.dispose();
    super.dispose();
  }

  String _getTextFieldValue() {
    return myController.text;
  }

  void _changeTextFieldValue(String newValue) {
    myController.text += newValue;
  }

  void __resetTextFieldValue() {
    myController.text = "";
  }

  @override
  Widget build(BuildContext context) {
    final Cart cart = context.watch<Cart>();
    final _formKey = GlobalKey<FormState>();
    return Scaffold(
      body: Row(
        children: <Widget>[
          Expanded(
            child: SizedBox(
              child: Scaffold(
                body: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Form(
                    // Build a Form widget using the _formKey created above.
                    key: _formKey,
                    child: Container(
                      margin: const EdgeInsets.symmetric(vertical: 15.0),
                      child: TextFormField(
                        keyboardType: TextInputType.number,
                        inputFormatters: <TextInputFormatter>[
                          FilteringTextInputFormatter.digitsOnly
                        ],
                        controller: myController,
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return "";
                          }
                          if (cart.catalog
                                  .getProductByCode(int.tryParse(value)!) ==
                              null) {
                            return "Ce produit n'existe pas !";
                          }
                          return null;
                        },
                        decoration: InputDecoration(
                          hintStyle: const TextStyle(fontSize: 15),
                          hintText: "Code article",
                          filled: true,
                          fillColor: Colors.white,
                          enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8.0),
                              borderSide: BorderSide(
                                  color: Colors.white.withOpacity(1))),
                          suffixIcon: Container(
                              decoration: const BoxDecoration(
                                  color: Color(0xFF2E4C6D),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(8.0))),
                              child: IconButton(
                                icon:
                                    const Icon(Icons.done, color: Colors.white),
                                padding: const EdgeInsets.all(12),
                                // borderRadius: BorderRadius.circular(8.0),
                                // TODO : put radius on validate icon
                                onPressed: () {
                                  if (_getTextFieldValue().isNotEmpty) {
                                    // Validate returns true if the form is valid, or false otherwise.
                                    if (_formKey.currentState!.validate()) {
                                      // If the form is valid, display a snackbar. In the real world,
                                      // you'd often call a server or save the information in a database.
                                      ScaffoldMessenger.of(context)
                                          .showSnackBar(
                                        const SnackBar(
                                            elevation: 6.0,
                                            behavior: SnackBarBehavior.floating,
                                            duration: Duration(seconds: 2),
                                            backgroundColor: Colors.green,
                                            content: Text('Produit ajouté')),
                                      );
                                    }
                                    cart.addProduct(
                                        int.tryParse(_getTextFieldValue())!, 1);
                                    __resetTextFieldValue();
                                  }
                                },
                              )),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
          Expanded(
              child: SizedBox(
                  width: 90.0,
                  height: 270,
                  child: GridView.builder(
                      itemCount: buttons.length,
                      gridDelegate:
                          const SliverGridDelegateWithFixedCrossAxisCount(
                              crossAxisCount: 3),
                      itemBuilder: (BuildContext context, int index) {
                        return SizedBox(
                            width: 5,
                            height: 1,
                            child: Padding(
                                padding: const EdgeInsets.all(
                                    5), // space between btn

                                child: ElevatedButton(
                                  child: Text(buttons[index],
                                      style: const TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 22)),
                                  // width and height

                                  onPressed: () {
                                    var value = buttons[index].toString();
                                    if (value == "X") {
                                      __resetTextFieldValue();
                                      return;
                                    }
                                    if (value == "->") {
                                      return;
                                    }
                                    _changeTextFieldValue(buttons[index]);
                                  },

                                  style: ElevatedButton.styleFrom(
                                    primary: const Color(0xFF2E4C6D),
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(10.0),
                                    ),
                                  ),
                                )));
                      })))
        ],
      ),
    );
  }
}
