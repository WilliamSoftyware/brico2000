import 'package:flowscan/common/theme.dart';
import 'package:flowscan/models/cart.dart';
import 'package:flowscan/models/catalog.dart';
import 'package:flowscan/screens/index.dart';
import 'package:flowscan/screens/login.dart';
import 'package:flowscan/screens/payment_modal.dart';
import 'package:flowscan/screens/buttons_code_product.dart';
import 'package:flowscan/screens/table_basket.dart';
import 'package:flowscan/screens/total_payment.dart';
import 'package:flowscan/screens/scan_button.dart';
import 'package:flowscan/screens/warning_modal.dart';
import 'package:flutter/material.dart';
import 'package:flowscan/paths_app.dart';
import 'my_app.dart';

class FlowScan extends StatelessWidget {
  const FlowScan({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => Catalog()),
        ChangeNotifierProxyProvider<Catalog, Cart>(
          create: (context) => Cart(),
          update: (context, catalog, cart) {
            if (cart == null) throw ArgumentError.notNull('cart');
            cart.catalog = catalog;
            return cart;
          },
        ),
      ],
      child: MaterialApp(
        // Remove the "debug" banner on the right side of the screen
        debugShowCheckedModeBanner: false,
        title: 'Brico2000',
        theme: appTheme,
        initialRoute: '/',
        routes: {
          '/': (context) => const Login(),
          '/index': (context) => const Index(),
          '/home': (context) => const MyApp(),
          '/refArticle': (context) => const RefArticle(),
          '/state': (context) => const ProductCart(),
          '/total-payment': (context) => const TotalPayment(),
          '/scan': (context) => const ScanButton(),
          '/warning-modal': (context) => const WarningModal(),
          '/pay': (context) => const PaymentModal(),
        },
      ),
    );
  }
}
