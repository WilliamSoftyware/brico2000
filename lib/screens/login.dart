import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class Login extends StatefulWidget {
  const Login({Key? key}) : super(key: key);
  @override
  _Login createState() => _Login();
}

class _Login extends State<Login> {
  final List<String> buttons = [
    "7",
    "8",
    "9",
    "4",
    "5",
    "6",
    "1",
    "2",
    "3",
    "0",
    "X",
    "Valider"
  ];

  // Create a text controller and use it to retrieve the current value
  // of the TextField.
  final myController = TextEditingController();

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    myController.dispose();
    super.dispose();
  }

  // String _getTextFieldValue() {
  //   return myController.text;
  // }

  void _changeTextFieldValue(String newValue) {
    myController.text += newValue;
  }

  void __resetTextFieldValue() {
    myController.text = "";
  }

  @override
  Widget build(BuildContext context) {
    final _formKey = GlobalKey<FormState>();
    return Scaffold(
        body: Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        //crossAxisAlignment: CrossAxisAlignment.center,

        children: <Widget>[
          Expanded(
            flex: 1,
            child: Container(
              margin: const EdgeInsets.fromLTRB(0, 150, 0, 0),
              width: 400,
              child: Scaffold(
                body: //Padding(
                    //padding: const EdgeInsets.all(14),
                    // child:
                    Form(
                  // Build a Form widget using the _formKey created above.
                  key: _formKey,
                  child: TextFormField(
                    keyboardType: TextInputType.number,
                    inputFormatters: <TextInputFormatter>[
                      FilteringTextInputFormatter.digitsOnly
                    ],
                    controller: myController,
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Veuillez saisir un code';
                      }
                      if (value.length > 4 || value != '1234') {
                        return 'Le code est incorrect';
                      }
                      return null;
                    },
                    decoration: InputDecoration(
                      hintText: "Code",
                      filled: true,
                      fillColor: Colors.white,
                      enabledBorder: OutlineInputBorder(
                          borderSide:
                              BorderSide(color: Colors.white.withOpacity(1))),
                    ),
                  ),
                ),
                //),
              ),
            ),
          ),
          Expanded(
              flex: 3,
              child: SizedBox(
                  width: 350,
                  child: GridView.builder(
                      itemCount: buttons.length,
                      gridDelegate:
                          const SliverGridDelegateWithFixedCrossAxisCount(
                              crossAxisCount: 3),
                      itemBuilder: (BuildContext context, int index) {
                        return Padding(
                            padding: const EdgeInsets.all(12.0),
                            child: ElevatedButton(
                              child: Text(
                                buttons[index],
                                style: const TextStyle(
                                    color: Colors.white, fontSize: 19),
                              ),
                              onPressed: () {
                                if (buttons[index].toString() == "X") {
                                  __resetTextFieldValue();
                                }
                                if (buttons[index].toString() != "Valider" &&
                                    buttons[index].toString() != "X") {
                                  _changeTextFieldValue(buttons[index]);
                                }
                                // Validate returns true if the form is valid, or false otherwise.
                                if (buttons[index].toString() == "Valider" &&
                                    _formKey.currentState!.validate() &&
                                    myController.text == "1234") {
                                  Navigator.pushReplacementNamed(
                                      context, '/home');
                                }
                              },
                              style: ElevatedButton.styleFrom(
                                primary: const Color(0xFF2E4C6D),
                              ),
                            ));
                      }))),
        ],
      ),
    ));
  }
}
