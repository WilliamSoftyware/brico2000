import 'package:flowscan/models/cart.dart';
import 'package:flutter/material.dart';
import 'package:flowscan/paths_app.dart';
import 'package:flutter/services.dart';

class ProductCart extends StatefulWidget {
  const ProductCart({Key? key}) : super(key: key);
  @override
  _ProductCartState createState() => _ProductCartState();
}

class _ProductCartState extends State<ProductCart> {
  _dismissDialog(context) {
    Navigator.pop(context);
  }

  void _deleteBasket(context, Cart cart) {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: const Text("Abandon d'un ticket"),
            content: const Text('Voulez vous vraiment abandonner le ticket ?'),
            actions: <Widget>[
              TextButton(
                  onPressed: () {
                    cart.cleanShoppingCart();
                    _dismissDialog(context);
                  },
                  child: const Text('Oui')),
              TextButton(
                onPressed: () {
                  _dismissDialog(context);
                },
                child: const Text('Non'),
              )
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    final Cart cart = context.watch<Cart>();
    return Scaffold(
        body: Container(
      color: const Color(0xFFEFEFEF),
      padding: const EdgeInsets.all(8.0),
      child: Column(
        children: <Widget>[
          Container(
            color: const Color(0xFF716F6F),
            child: Row(
              children: <Widget>[
                Container(
                    padding: const EdgeInsets.all(7.0),
                    width: 210.0,
                    child: const Text(
                      "Référence",
                      style: TextStyle(fontSize: 18, color: Colors.white),
                    )),
                Container(
                    padding: const EdgeInsets.all(7.0),
                    width: 205.0,
                    child: const Text(
                      "Nom",
                      style: TextStyle(fontSize: 18, color: Colors.white),
                    )),
                Container(
                    padding: const EdgeInsets.all(7.0),
                    width: 176.0,
                    child: const Text(
                      "Quantité",
                      style: TextStyle(fontSize: 18, color: Colors.white),
                    )),
                Container(
                    padding: const EdgeInsets.all(7.0),
                    width: 140.0,
                    child: const Text(
                      "Prix",
                      style: TextStyle(fontSize: 18, color: Colors.white),
                    )),
              ],
            ),
          ),
          Expanded(child: _CartList()),
          Expanded(
              child: SizedBox(
                  child: ButtonBar(
            alignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              SizedBox(
                width: 180,
                height: 50,
                child: ElevatedButton(
                  child: const Text('Abandonner'),
                  style: ElevatedButton.styleFrom(
                      primary: Colors.deepOrange[800],
                      textStyle: const TextStyle(
                        fontSize: 20.0,
                        color: Colors.white,
                      )),
                  onPressed: () => _deleteBasket(context, cart),
                ),
              ),
              SizedBox(
                width: 180,
                height: 50,
                child: ElevatedButton(
                  child: const Text('Mise en attente'),
                  style: ElevatedButton.styleFrom(
                      primary: Colors.orange[800],
                      textStyle: const TextStyle(
                        fontSize: 20.0,
                        color: Colors.white,
                      )),
                  onPressed: () => {},
                ),
              ),
            ],
          ))),
        ],
      ),
    ));
  }
}

class _CartList extends StatelessWidget {
  void _deleteProduct(context, int index, Cart cart) {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: const Text("Suppression d'un produit"),
            content: const Text('Voulez vous vraiment supprimer le produit ?'),
            actions: <Widget>[
              TextButton(
                  onPressed: () {
                    _dismissDialog(context);
                    cart.putBack(cart.getProductList()[index].productCode,
                        cart.getProductList()[index].quantity);
                  },
                  child: const Text('Oui')),
              TextButton(
                onPressed: () {
                  _dismissDialog(context);
                },
                child: const Text('Non'),
              )
            ],
          );
        });
  }

  _dismissDialog(context) {
    Navigator.pop(context);
  }

  // Item of the ListView
  Widget _listProduct(context, index, Cart cart,
      List<TextEditingController> _textFieldQtyControllers) {
    return Container(
      child: ListTile(
        title: Row(children: <Widget>[
          Expanded(
              child: Text(cart.getProductList()[index].productCode.toString(),
                  style: const TextStyle(fontSize: 18, color: Colors.black))),
          const Expanded(
            child: Text(""),
          ),
          Expanded(
              child: Text(cart.getProductList()[index].name,
                  style: const TextStyle(fontSize: 18, color: Colors.black))),
          const Expanded(
            child: Text(""),
          ),
          Expanded(
            child: TextFormField(
              keyboardType: TextInputType.number,
              inputFormatters: <TextInputFormatter>[
                FilteringTextInputFormatter.digitsOnly
              ],
              controller: _textFieldQtyControllers[index],
              onChanged: (value) => {
                if (value.isNotEmpty)
                  {
                    cart.changeQuantity(
                        cart.getProductList()[index].productCode,
                        int.tryParse(value)!),
                  }
              },
              textAlign: TextAlign.center,
              decoration: InputDecoration(
                  isDense: true, // important line
                  contentPadding: const EdgeInsets.fromLTRB(
                      10, 12, 10, 2), // control your hints text size
                  hintStyle: const TextStyle(
                      letterSpacing: 2,
                      color: Colors.black54,
                      fontWeight: FontWeight.bold),
                  fillColor: Colors.white,
                  filled: true,
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(5),
                  )),
            ),
          ),
          // Space ?
          const Expanded(
            child: Text(""),
          ),
          Expanded(
              child: Text(
                  cart
                          .getTotalProductPrice(
                              cart.getProductList()[index].productCode)
                          .toString() +
                      " €",
                  style: const TextStyle(fontSize: 18, color: Colors.black))),
        ]),
        trailing: ElevatedButton(
          child: const Icon(Icons.close_outlined),
          style: ElevatedButton.styleFrom(
              primary: const Color(0xFF2E4C6D),
              textStyle: const TextStyle(
                color: Colors.white,
              )),
          onPressed: () {
            _deleteProduct(context, index, cart);
          },
        ),
      ),
      decoration: const BoxDecoration(
        border: Border(bottom: BorderSide(width: 1, color: Color(0xFFEFEFEF))),
        color: Colors.white,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    List<TextEditingController> _textFieldQtyControllers = [];
    final Cart cart = context.watch<Cart>();
    return Scaffold(
        body: ListView.builder(
      itemCount: cart.getProductList().length,
      itemBuilder: (context, index) {
        _textFieldQtyControllers.add(TextEditingController(
            text: cart
                .getQuantity(cart.getProductList()[index].productCode)
                .toString()));
        return _listProduct(context, index, cart, _textFieldQtyControllers);
      },
    ));
  }
}
