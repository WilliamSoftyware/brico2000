import 'package:flowscan/models/cart.dart';
import 'package:flutter/material.dart';
import 'package:flowscan/paths_app.dart';

class ScanButton extends StatelessWidget {
  const ScanButton({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final Cart cart = context.watch<Cart>();
    return Scaffold(
        body: Container(
            padding: const EdgeInsets.fromLTRB(40, 10, 40, 10),
            margin: const EdgeInsets.fromLTRB(40, 10, 40, 10),
            color: const Color(0xFFFFFFFF),
            child: Center(
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          const SizedBox(),
                          SizedBox(
                              width: 300,
                              height: 50,
                              child: Center(
                                  child: ElevatedButton(
                                      key: null,
                                      onPressed: () {
                                        cart.addProduct(9000, 1);
                                      },
                                      style: ButtonStyle(
                                          backgroundColor:
                                              MaterialStateProperty.all(
                                                  const Color(0xFFFFB800))),
                                      child: const Text(
                                        "Scanner",
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            fontSize: 31.0,
                                            color: Color(0xFFffffff),
                                            fontWeight: FontWeight.w400,
                                            fontFamily: "Roboto"),
                                      ))))
                        ])
                  ]),
            )));
  }
}
