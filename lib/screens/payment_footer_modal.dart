import 'package:flutter/material.dart';

class PaymentFooterModal extends StatelessWidget {
  const PaymentFooterModal({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(

      body:  Container(
        padding: const EdgeInsets.fromLTRB(40, 10, 40, 10),
    margin: const EdgeInsets.fromLTRB(40, 10, 40, 10),
    color: const Color(0xFFFFFFFF),
    child: Center(
    child: Column(
    mainAxisAlignment: MainAxisAlignment.end,
    mainAxisSize: MainAxisSize.min,
    crossAxisAlignment: CrossAxisAlignment.center,
    children: <Widget>[
      Row(
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(
                width: 300,
                height: 50,
                child: ElevatedButton(
                    key: null,
                    onPressed: buttonPressed,
                    style: ButtonStyle(
                        backgroundColor:
                            MaterialStateProperty.all(const Color(0xFF2BA84A))),
                    child: const Text(
                      "Payer",
                      style: TextStyle(
                          fontSize: 31.0,
                          color: Color(0xFFffffff),
                          fontWeight: FontWeight.w400,
                          fontFamily: "Roboto"),
                    )
                )
    )
        ]
    )

          ]
    ),
    )));
  }

  void buttonPressed() {}
}
