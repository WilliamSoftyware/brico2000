import 'package:flutter/material.dart';

class WarningModal extends StatelessWidget {
  const WarningModal({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: const <Widget>[
              Icon(Icons.cancel, color: Color(0xFFe84e4e), size: 100.0),
              Text(
                "Caisse ouverte",
                style: TextStyle(
                    fontSize: 25.0,
                    color: Color(0xFF2E4C6D),
                    fontWeight: FontWeight.w700,
                    fontFamily: "Roboto"),
              ),
              Text(
                "Veuillez fermer le tiroir pour continuer à utiliser la caisse",
                style: TextStyle(
                    fontSize: 22.0,
                    color: Color(0xFF2E4C6D),
                    fontWeight: FontWeight.w200,
                    fontFamily: "Roboto"),
              )
            ]),
        padding: const EdgeInsets.all(0.0),
        alignment: Alignment.center,
      ),
    );
  }

  void buttonPressed() {}
}