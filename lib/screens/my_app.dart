import 'package:flowscan/screens/buttons_code_product.dart';
import 'package:flowscan/screens/scan_button.dart';
import 'package:flowscan/screens/table_basket.dart';
import 'package:flowscan/screens/total_payment.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final ButtonStyle style = TextButton.styleFrom(primary: Colors.white);
    return MaterialApp(
        // Remove the "debug" banner on the right side of the screen
        debugShowCheckedModeBanner: false,
        title: 'Brico 2000',
        // Background color of the app
        theme: ThemeData(scaffoldBackgroundColor: const Color(0xFFEFEFEF)),
        home: Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.grey[700],
            actions: <Widget>[
              TextButton(
                style: style,
                onPressed: () {},
                child: Text(DateFormat.MEd().add_Hm().format(DateTime.now())),
              ),
              IconButton(
                  icon:
                      const Icon(Icons.power_settings_new, color: Colors.white),
                  onPressed: () => showDialog<String>(
                      context: context,
                      builder: (BuildContext context) => AlertDialog(
                            title: const Text('Déconnexion ?'),
                            actions: <Widget>[
                              TextButton(
                                  style: TextButton.styleFrom(
                                    primary: Colors.black45,
                                  ),
                                  onPressed: () =>
                                      Navigator.pop(context, 'Cancel'),
                                  child: const Text(
                                    'Annuler',
                                    textAlign: TextAlign.left,
                                    style: TextStyle(
                                      color: Colors.black54,
                                    ),
                                  )),
                              TextButton(
                                style: TextButton.styleFrom(
                                  primary: Colors.green,
                                ),
                                //onPressed: () => Navigator.pop(context, 'OK'),
                                onPressed: () {
                                  Navigator.pushReplacementNamed(context, '/');
                                },
                                child: const Text('Valider'),
                              ),
                            ],
                          )))
            ],
            title: const Center(
              child: Text('Bonjour, Test'),
            ),
            leading: Builder(
              builder: (BuildContext context) {
                return IconButton(
                  icon: const Icon(
                    Icons.more_horiz,
                    color: Colors.white,
                  ),
                  onPressed: () {
                    Scaffold.of(context).openDrawer();
                  },
                  tooltip:
                      MaterialLocalizations.of(context).openAppDrawerTooltip,
                );
              },
            ),
          ),
          body: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Container(
                padding: const EdgeInsets.symmetric(vertical: 30),
                width: 800,
                child: const ProductCart(),
                //decoration: BoxDecoration(color: Colors.grey),
              ),
              Column(
                children: [
                  //Container(height: 20, width: 400),
                  Container(
                      margin: const EdgeInsets.symmetric(
                          vertical: 30, horizontal: 30),
                      height: 300,
                      width: 410,
                      child: const RefArticle()),
                  const SizedBox(
                    height: 100,
                    width: 550,
                    child: ScanButton(),
                  ),
                  const SizedBox(height: 200, width: 550, child: TotalPayment())
                ],
              )
            ],
          ),
        ));
  }
}
