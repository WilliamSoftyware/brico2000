import 'package:flowscan/models/cart.dart';
import 'package:flowscan/paths_app.dart';
import 'package:flutter/material.dart';

class TotalPayment extends StatelessWidget {
  const TotalPayment({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final Cart cart = context.watch<Cart>();
    return Scaffold(
        body: Container(
      padding: const EdgeInsets.fromLTRB(95, 20, 95, 20),
      margin: const EdgeInsets.fromLTRB(40, 20, 40, 20),
      color: const Color(0xFFFFFFFF),
      child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Row(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Container(
                    width: 140,
                    padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
                    decoration: BoxDecoration(
                        color: const Color(0xFFEFEFEF),
                        border: Border.all(
                            width: 1.5, color: const Color(0xFFCCCCCC))),
                    child: const Text(
                      "Total : ",
                      textAlign: TextAlign.left,
                      style: TextStyle(
                          fontSize: 25.0,
                          color: Color(0xFF000000),
                          fontWeight: FontWeight.w200,
                          fontFamily: "Roboto"),
                    ),
                  ),
                  Container(
                    width: 140,
                    padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
                    decoration: const BoxDecoration(
                        color: Color(0xFFEFEFEF),
                        border: Border(
                          top: BorderSide(width: 1.5, color: Color(0xFFCCCCCC)),
                          right:
                              BorderSide(width: 1.5, color: Color(0xFFCCCCCC)),
                          bottom:
                              BorderSide(width: 1.5, color: Color(0xFFCCCCCC)),
                        )),
                    child: Text(
                      cart.getTotalPrice().toStringAsFixed(2) + " €",
                      textAlign: TextAlign.right,
                      style: const TextStyle(
                          fontSize: 25.0,
                          color: Color(0xFF000000),
                          fontWeight: FontWeight.w400,
                          fontFamily: "Roboto"),
                    ),
                  )
                ]),
            Row(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  const SizedBox(
                    height: 68,
                  ),
                  SizedBox(
                      width: 280,
                      height: 50,
                      child: ElevatedButton(
                          onPressed: () {
                            Navigator.pushNamed(context, '/pay');
                          },
                          style: ButtonStyle(
                              backgroundColor: MaterialStateProperty.all(
                                  const Color(0xFF2BA84A))),
                          child: const Text(
                            "Payer",
                            style: TextStyle(
                                fontSize: 31.0,
                                color: Color(0xFFffffff),
                                fontWeight: FontWeight.w400,
                                fontFamily: "Roboto"),
                          )))
                ])
          ]),
    ));
  }
}
