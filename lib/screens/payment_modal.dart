import 'package:flowscan/screens/payment_footer_modal.dart';
import 'package:flutter/material.dart';

class PaymentModal extends StatelessWidget {
  const PaymentModal({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              const Text(
                "Sélectionnez un ou des moyens de paiement",
                style: TextStyle(
                    fontSize: 25.0,
                    color: Color(0xFF2D353E),
                    fontWeight: FontWeight.w700,
                    fontFamily: "Roboto"),
              ),
              Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Image.network(
                      'https://upload.wikimedia.org/wikipedia/fr/7/72/Logo_GIE-CB.jpg',
                      fit: BoxFit.fill,
                      width: 250,
                      height: 170,
                    ),
                    Image.network(
                      'https://thumbs.dreamstime.com/b/gagnez-la-conception-d-ic%C3%B4ne-de-logo-vecteur-argent-symbole-salaire-avec-l-illustration-main-des-illustrations-banque-affaires-152893719.jpg',
                      fit: BoxFit.fill,
                      width: 250,
                      height: 170,
                    ),
                    Image.network(
                      'https://previews.123rf.com/images/joseelias/joseelias1802/joseelias180200110/95807421-ic%C3%B4ne-de-devise-euro-ou-vecteur-de-logo-sur-un-ch%C3%A8que-de-paie-ou-un-ch%C3%A8que-symbole-pour-la-banque-de.jpg',
                      fit: BoxFit.fill,
                      width: 250,
                      height: 170,
                    ),
                  ]),
              const SizedBox(height: 50),
              const PaymentFooterModal(),
            ]),
        padding: const EdgeInsets.all(0.0),
        alignment: Alignment.center,
      ),
    );
  }

  void buttonPressed() {}
}
